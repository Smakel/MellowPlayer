//
// This file is part of MellowPlayer.
//
// MellowPlayer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// MellowPlayer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with MellowPlayer.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

function getButtons() {
    return {
        playButton: document.getElementsByClassName("animated-play-button play_pause_button play_button")[0],
        pauseButton: document.getElementsByClassName("animated-play-button play_pause_button pause_button")[0],
        skipForward: document.getElementsByClassName("skip-forward-button")[0],
        skipBack: document.getElementsByClassName("skip-back-button")[0],
        progressBar: document.getElementsByClassName("seek-bar")[0]
    };
}

// getElementsByClass returns empty if the class doesn't exist
// Pocketcasts changes class "seek-bar" to "seek-bar-buffering" when content is buffering
function isBuffering() {
    var buffering = document.getElementsByClassName("seek-bar buffering");
    if (buffering === 0) {
        return true;
    }
}
// var loaded = false;
// function isLoaded(){
//     loaded = true;
// }
// window.onload = isLoaded();

function isPlaying() {
    //if(loaded){
    var list = document.querySelector(".animated-play-button");
    if (list.classList.contains("pause_button")) {
        return true;
    }
    else {
        return false;
    }
}
    else{
        return false;
    }
}

// Next two functions taken from https://gitlab.com/ColinDuquesnoy/MellowPlayer/tree/master/src/plugins/googleplaymusic
/**
 * @param element    (HTMLElement) Element to send events to. 
 * @param eventName  (String) type of the MouseEvent to send. 
 * @param relativeX  (Float) relative x position within the boundaries of the element, 
 * as a fraction of the element's width(0..1). 
 * @param relativeY  (Float) relative y position within the boundaries of the element,
 * as a fraction of the element's height(0..1).
*/
function sendMouseEventToElement(element, eventName, relativeX, relativeY) {
    var clientRect = element.getBoundingClientRect();
    var event = new MouseEvent(eventName, {
        'view': window,
        'bubbles': true,
        'cancelable': true,
        'clientX': clientRect.left + (clientRect.width * relativeX),
        'clientY': clientRect.top + (clientRect.height * relativeY)
    });
    element.dispatchEvent(event);
}

/**
 * Emulates mouse click on the specified position of the given element
 * @param element    (HTMLElement) Element to send click to. 
 * @param relativeX  (Float) relative x position within the boundaries of the element, 
 * as a fraction of the element's width(0..1). 
 * @param relativeY  (Float) relative y position within the boundaries of the element,
 * as a fraction of the element's height(0..1).
*/
function sendMouseClickToElement(element, relativeX, relativeY) {
    sendMouseEventToElement(element, 'mouseover', relativeX, relativeY);
    sendMouseEventToElement(element, 'mousedown', relativeX, relativeY);
    sendMouseEventToElement(element, 'mouseup', relativeX, relativeY);
}

function getTime() {
    // Get the two relevant elements and their values
    var played = document.getElementsByClassName("time-text")[0].innerHTML;
    var remaining = document.getElementsByClassName("time-text")[1].innerHTML;
    // Trim the negative sign off time remaining
    remaining = remaining.slice(1);
    played = toSeconds(played);
    remaining = toSeconds(remaining);
    // Add them together to get the duration 
    var duration = remaining + played;
    return {
        position: played,
        duration: duration,
    };
}

// Gets volume directly from HTMLAudioElement 
function getVolume() {
    var elm = document.getElementsByClassName("audio")[0];
    return elm.volume;
}

function updateVolumeBar(volume) {
    var knob = document.getElementsByClassName("knob")[1];
    var fill = document.getElementsByClassName("fill")[0];
    var knobRelPos = 49 * volume;
    var fillRelPos = 55 * volume;
    var fillString = ("width: " + fillRelPos + "px;");
    var knobString = ("left: " + knobRelPos + "px;");
    knob.setAttribute("style", knobString);
    fill.setAttribute("style", fillString);
}

function update() {
    var playbackStatus = mellowplayer.PlaybackStatus.STOPPED;
    if (isPlaying()) {
        playbackStatus = mellowplayer.PlaybackStatus.PLAYING;
    }
    else if (isBuffering()) {
        playbackStatus = mellowplayer.PlaybackStatus.BUFFERING;
    }
    else {
        playbackStatus = mellowplayer.PlaybackStatus.PAUSED;
    }

    // Get the podcast name
    var podName = document.getElementsByClassName("episode-title player_episode")[0].innerHTML;
    // Get the podcast art
    var elementArr = document.getElementsByClassName("player-gradient");
    var artElement = elementArr[0];
    artElement = artElement.getAttribute("style");
    var podArt = artElement.match(/"(.*?)"/)[1];
    // Get podcast author name
    var podAuth = document.getElementsByClassName("podcast-title player_podcast_title")[0].innerHTML;





    return {
        "playbackStatus": playbackStatus,
        "canSeek": true,
        "canGoNext": true,
        "canGoPrevious": true,
        "canAddToFavorites": false,
        "volume": getVolume(),
        "duration": getTime().duration,
        "position": getTime().position,
        "songId": getHashCode(podName),
        "songTitle": podName,
        "artistName": podAuth,
        "albumTitle": "",
        "artUrl": podArt,
        "isFavorite": false

    };
}

function play() {
    getButtons().playButton.click();
}


function pause() {
    getButtons().pauseButton.click();
}

function goNext() {
    getButtons().skipForward.click();
}

function goPrevious() {
    getButtons().skipBack.click();
}

function seekToPosition(position) {
    var posFraction = position / getTime().duration;
    sendMouseClickToElement(getButtons().progressBar, posFraction, 0.5);
}

function setVolume(volume) {
    // Directly update volume 
    var elm = document.getElementsByClassName("audio")[0];
    elm.volume = volume;
    // Update the visual aspect 
    updateVolumeBar(volume);
}

function addToFavorites() {
    // not supported
}

function removeFromFavorites() {
    // not supported
}